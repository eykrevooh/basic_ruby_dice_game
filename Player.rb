class Player
 attr_accessor :name, :value
 def initialize(name, value)
   @name = name
   @value = value.to_i
 end

 def add_value(amount)
   @value += amount
 end

 def remove_value(amount)
   @value -= amount
 end 
end
