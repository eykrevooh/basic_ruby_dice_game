require_relative "Player"
require_relative "Dice"
require "highline/import"

class Game
  def initialize(number_players, target_number)
    #Create Players
    @player_list = Array.new(number_players - 1)
    for i in 0..(number_players-1)
      @player_list[i] = Player.new((ask "Player Name:"), (ask "Player Start Money:"))
    end
    #Initialize other variables
    @target_number = target_number
    @dice1 = Dice.new(6)
    @dice2 = Dice.new(6) 
  end

  def get_bet(player)
    bet = (ask player.name + " you have $" + player.value.to_s + ", how much would you like to bet on this roll?")
    while bet.to_i > player.value()
      bet = (ask "You do not have that much, you have $" + player.value.to_s  + ", how much would you like to bet on this roll?")
    end
    bet.to_i
  end

  def play_game
    while @player_list[0].value != 0 do #This does not scale to multiple players
      for player in @player_list do
        amount_bet = get_bet(player) 
        dice_roll = @dice1.roll() + @dice2.roll()
        if dice_roll != @target_number
          puts "Sorry you rolled a "+ dice_roll.to_s + ", you lost $" + amount_bet.to_s
          player.remove_value(amount_bet)
        else
          puts "Congrats you rolled a "+ dice_roll.to_s + ", you won $" + amount_bet.to_s
          player.add_value(amount_bet)
        end
      end
    end
    puts "You are out of money b!#$@#! Get the f*#! out"
  end
end

game = Game.new(1, 7)
game.play_game()
